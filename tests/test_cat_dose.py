# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool


class CategoryDoseTestCase(ModuleTestCase):
    """Test dose product category module"""
    module = 'product_uom_cat_dose'

    @with_transaction()
    def test_get_dose_divisor_unit(self):
        'Test get dose divisor unit'
        pool = Pool()
        Uom = pool.get('product.uom')
        ModelData = pool.get('ir.model.data')

        tests = [
            (
                [
                    'Kilogram per Hectare',
                    'Liter per Hectare',
                    'Cubic meter per Hectare',
                    'Units per Hectare'
                ], 'Hectare'
            ),
            (
                [
                    'Gram per Liter',
                    'Cubic centimeter per Liter',
                ], 'Liter'
            ),
            (
                [
                    'Kilogram per cubic meter',
                    'Liter per cubic meter'
                ], 'Cubic meter'
            ),
            (
                [
                    'Gram per 100 Liter',
                    'Cubic centimeter per 100 Liter',
                ], '100 Liter'
            )
        ]

        for uoms_name, divisor_name in tests:
            if divisor_name == '100 Liter':
                divisor_uom = Uom(ModelData.get_id(
                    'product_uom_cat_dose', 'uom_100_liter'))
            else:
                divisor_uom, = Uom.search([
                        ('name', '=', divisor_name),
                        ], limit=1)

            for uom_name in uoms_name:
                uom, = Uom.search([
                    ('name', '=', uom_name),
                    ], limit=1)

                self.assertEqual(uom._get_dose_divisor_unit(), divisor_uom)

    @with_transaction()
    def test_get_dose_dividend_unit(self):
        'Test get dose dividend unit'
        pool = Pool()
        Uom = pool.get('product.uom')

        tests = [
            (
                [
                    'Kilogram per Hectare',
                    'Kilogram per cubic meter',
                ], 'Kilogram'
            ),
            (
                [
                    'Liter per Hectare',
                    'Liter per cubic meter'
                ], 'Liter'
            ), (
                [
                    'Gram per Liter',
                    'Gram per 100 Liter',
                ], 'Gram'
            ),
            (
                [
                    'Cubic centimeter per Liter',
                    'Cubic centimeter per 100 Liter'
                ], 'Cubic centimeter'
            ),
            (
                [
                    'Cubic meter per Hectare',
                ], 'Cubic meter'
            ),
            (
                [
                    'Units per Hectare',
                ], 'Unit'
            )
        ]

        for uoms_name, dividend_name in tests:
            dividend_uom, = Uom.search([
                    ('name', '=', dividend_name),
                    ], limit=1)
            for uom_name in uoms_name:
                uom, = Uom.search([
                    ('name', '=', uom_name),
                    ], limit=1)

                self.assertEqual(uom._get_dose_dividend_unit(), dividend_uom)

    @with_transaction()
    def test_compute_qty(self):
        'Test compute_qty'
        pool = Pool()
        Uom = pool.get('product.uom')
        ModelData = pool.get('ir.model.data')

        uom_100l = Uom(ModelData.get_id(
            'product_uom_cat_dose', 'uom_100_liter'))
        uom_l = Uom(ModelData.get_id('product', 'uom_liter'))

        self.assertEqual(100, Uom.compute_qty(uom_100l, 1, uom_l))
        self.assertEqual(1, Uom.compute_qty(uom_l, 100, uom_100l))
        self.assertEqual(1, Uom.compute_qty(uom_100l, 0.01, uom_l))
        self.assertEqual(0.01, Uom.compute_qty(uom_l, 1, uom_100l))

    @with_transaction()
    def test_uom_compute_dose_qty(self):
        'Test uom compute_dose_qty function'
        pool = Pool()
        Uom = pool.get('product.uom')
        ModelData = pool.get('ir.model.data')

        # dose_quantity, factor, _dividend_name, factor_uom_name,
        # to_uom_name, result
        tests = [
            (15, 100, 'Kilogram per Hectare', 'Hectare', None, 1500),
            (10, 5, 'Liter per Hectare', 'Hectare', None, 50),
            (15, 100, 'Kilogram per Hectare', 'Hectare', 'Gram', 1500000),
            (200, 3000, 'Cubic centimeter per Liter', 'Cubic meter', 'Liter', 0.6),
            (50, 2, 'Kilogram per cubic meter', 'Cubic meter', None, 100),
            (2, 5, 'Kilogram per cubic meter', 'Cubic meter', 'Gram', 10000),
            (20, 5, 'Liter per cubic meter', 'Cubic meter', None, 100),
            (1, 80, 'Liter per cubic meter', 'Cubic meter', 'Cubic meter', 0.08),
            (10, 100, 'Gram per 100 Liter', '100 Liter', None, 1000),
            (10, 1, 'Gram per 100 Liter', '100 Liter', None, 10),
            (10, 1, 'Gram per 100 Liter', '100 Liter', 'Kilogram', 0.01),
            (3, 100, 'Gram per 100 Liter', 'Liter', 'Kilogram', 30),
            (10, 10, 'Cubic centimeter per 100 Liter', '100 Liter', None, 100),
            (8, 10, 'Cubic centimeter per 100 Liter', '100 Liter', 'Liter', 0.08)
        ]

        for (dose_quantity, factor, _dividend_name, factor_uom_name,
                to_uom_name, result) in tests:
            _dividend, = Uom.search([
                    ('name', '=', _dividend_name),
                    ], limit=1)
            if factor_uom_name == '100 Liter':
                factor_uom = Uom(ModelData.get_id(
                    'product_uom_cat_dose', 'uom_100_liter'))
            else:
                factor_uom, = Uom.search([
                        ('name', '=', factor_uom_name),
                        ], limit=1)

            to_uom = None
            if to_uom_name:
                to_uom, = Uom.search([
                        ('name', '=', to_uom_name),
                        ], limit=1)

            result2 = _dividend.compute_dose_qty(dose_quantity=dose_quantity,
                factor=factor, factor_uom=factor_uom, to_uom=to_uom)

            self.assertEqual(result, result2)

    @with_transaction()
    def test_uom_compute_dose(self):
        'Test uom compute_dose_qty function'
        pool = Pool()
        Uom = pool.get('product.uom')
        ModelData = pool.get('ir.model.data')

        # quantity, factor, _dividend_name, factor_uom_name, dose_uom, result
        tests = [
            (1500, 100, 'Kilogram', 'Hectare', 'Kilogram per Hectare', 15),
            (1500, 10000, 'Kilogram', 'Are', 'Kilogram per Hectare', 15),
            (50, 5, 'Liter', 'Hectare', 'Liter per Hectare', 10),
            (50, 500, 'Liter', 'Are', 'Liter per Hectare', 10),
            (1500000, 100, 'Gram', 'Hectare', 'Kilogram per Hectare', 15),
            (1500000, 10000, 'Gram', 'Are', 'Kilogram per Hectare', 15),
            (100, 1, 'Liter', 'Hectare', 'Kilogram per Hectare', 100),
            (100, 100, 'Liter', 'Are', 'Kilogram per Hectare', 100),
            (100, 2, 'Kilogram', 'Cubic meter', 'Kilogram per cubic meter', 50),
            (100, 2000, 'Kilogram', 'Liter', 'Kilogram per cubic meter', 50),
            (10000, 5, 'Gram', 'Cubic meter', 'Kilogram per cubic meter', 2),
            (10000, 5000, 'Gram', 'Liter', 'Kilogram per cubic meter', 2),
            (100, 5, 'Liter', 'Cubic meter', 'Liter per cubic meter', 20),
            (100, 5000, 'Liter', 'Liter', 'Liter per cubic meter', 20),
            (0.08, 1, 'Cubic meter', 'Cubic meter', 'Liter per cubic meter', 80),
            (0.08, 1000, 'Cubic meter', 'Liter', 'Liter per cubic meter', 80),
            (100, 50, 'Gram', '100 Liter', 'Gram per 100 Liter', 2),
            (100, 5000, 'Gram', 'Liter', 'Gram per 100 Liter', 2),
            (1, 20, 'Kilogram', '100 Liter', 'Gram per 100 Liter', 50),
            (1, 2000, 'Kilogram', 'Liter', 'Gram per 100 Liter', 50),
            (100, 10, 'Cubic centimeter', '100 Liter', 'Cubic centimeter per 100 Liter', 10),
            (100, 1000, 'Cubic centimeter', 'Liter', 'Cubic centimeter per 100 Liter', 10),
            (2, 1000, 'Liter', '100 Liter', 'Cubic centimeter per 100 Liter', 2),
            (2, 100000, 'Liter', 'Liter', 'Cubic centimeter per 100 Liter', 2),
        ]

        for (quantity, factor, _dividend_name, factor_uom_name, dose_uom_name,
                result) in tests:
            _dividend, = Uom.search([
                    ('name', '=', _dividend_name),
                    ], limit=1)
            if factor_uom_name == '100 Liter':
                factor_uom = Uom(ModelData.get_id(
                    'product_uom_cat_dose', 'uom_100_liter'))
            else:
                factor_uom, = Uom.search([
                        ('name', '=', factor_uom_name),
                        ], limit=1)
            dose_uom, = Uom.search([
                    ('name', '=', dose_uom_name),
                    ], limit=1)

            result2 = _dividend.compute_dose(
                quantity, factor, factor_uom, dose_uom)

            self.assertEqual(result, result2)

    @with_transaction()
    def test_dose_unit(self):
        'Test getting dose unit for a given uom'
        pool = Pool()
        Uom = pool.get('product.uom')

        tests = [
            ('Kilogram', 'Hectare', 'Kilogram per Hectare'),
            ('Liter', 'Hectare', 'Liter per Hectare'),
            ('Gram', 'Hectare', 'Kilogram per Hectare'),
            ('Gram', 'Liter', 'Gram per Liter'),
            ('Liter', 'Liter', 'Cubic centimeter per Liter'),
            ('Cubic meter', 'Hectare', 'Cubic meter per Hectare'),
            ('Unit', 'Hectare', 'Units per Hectare'),
            ('Kilogram', 'Cubic meter', 'Kilogram per cubic meter'),
            ('Liter', 'Cubic meter', 'Liter per cubic meter'),
            ('Cubic meter', 'Liter', 'Cubic centimeter per Liter')
        ]

        for qty_uom, factor_uom, dose_uom in tests:
            qty_uom, = Uom.search([
                    ('name', '=', qty_uom),
                ], limit=1)
            factor_uom, = Uom.search([
                    ('name', '=', factor_uom),
                ], limit=1)

            result = qty_uom._get_dose_unit(factor_uom)

            self.assertEqual(result.name, dose_uom)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        CategoryDoseTestCase))
    return suite
